from datetime import datetime
import json

from django.http import JsonResponse
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "attendees_bc.settings")
django.setup()

from attendees.models import AccountVO


def update_accountvo(ch, method, properties, body):
    content = json.loads(body)
    first_name = content["first_name"]
    last_name = content["last_name"]
    email = content["email"]
    is_active = content["is_active"]
    updated_string = content["updated_string"]
    updated = datetime.fromisoformat(updated_string)

    if is_active:
        account = AccountVO.objects.create(**content)
        return JsonResponse(account)
    else:
        count, _ = AccountVO.objects.filter(id).delete()
        return JsonResponse({"deleted": count > 0})
        # Delete the AccountVO object with the specified email, if it exists


# Based on the reference code at
#   https://github.com/rabbitmq/rabbitmq-tutorials/blob/master/python/receive_logs.py
# infinite
while True:
    try:
        parameters = pika.ConnectionParameters(host="rabbitmq")
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.exchange_declare(
            exchange="account_info", exchange_type="fanout"
        )
        channel.queue_declare(queue="account")
        channel.queue_bind(exchange="account_info", queue="account")
        channel.basic_consume(
            queue="account",
            on_message_callback=update_accountvo,
            auto_ack=True,
        )

        channel.start_consuming()
    except AMQPConnectionError:
        print(
            "Could not connect to RabbitMQ. Have it sleep for a couple of minutes."
        )
        time.sleep(2.0)