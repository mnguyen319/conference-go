from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests

# def get_location_pictures():

# # use requests. request 'response

#     response = requests.get("https://api.pexels.com/v1/")

#     pictures = json.loads(response.content)

# # convert to dict to use

#     pictures{
#         "picture_url": pictures["picture_url"],
#         }
#     return pictures
def find_pictures(search_term):
    url = "https://api.pexels.com/v1/search?query=" + search_term
    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.get(url, headers=headers)
    data = json.loads(response.content)

    photos = data["photos"]
    if photos and len(photos) > 0:
        return photos


# def get_weather_data(city, state):
#     url = (
#         "http://api.openweathermap.org/geo/1.0/direct?q="
#         + city
#         + ","
#         + state
#         + ",US&limit=5&appid="
#         + OPEN_WEATHER_API_KEY
#     )
#     response = requests.get(url)
#     data = json.loads(response.content)[0]
#     lon = data["lon"]
#     lat = data["lat"]

#     print(lon, lat)
#     return {}

#     return {}
