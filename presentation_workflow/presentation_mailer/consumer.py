import json
import time
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
from django.core.mail import send_mail

print("Hello")
sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()

# send_mail(
#     "Subject here",
#     "Here is the message.",
#     "from@example.com",
#     ["to@example.com"],
#     fail_silently=False,
# )


def process_approval(ch, method, properties, body):
    print("  Received %r" % body)
    send_mail(
        "Subject here",
        "Here is the message.",
        "from@example.com",
        ["to@example.com"],
        fail_silently=False,
    )
    # json.loads()


# def process_rejection(ch, method, properties, body):
#     print("  Received %r" % body)
#     send_mail(
#         "Subject here",
#         "Here is the message.",
#         "from@example.com",
#         ["to@example.com"],
#         fail_silently=False,
#     )

while True:
    try:
        parameters = pika.ConnectionParameters(host="rabbitmq")
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.queue_declare(queue="presentation_approvals")
        # channel.queue_declare(queue="presentation_rejections")
        channel.basic_consume(
            queue="presentation_approvals",
            on_message_callback=process_approval,
            auto_ack=True,
        )
        # channel.basic_consume(
        #     queue="presentation_rejections",
        #     on_message_callback=process_rejection,
        #     auto_ack=True,
        # )
        print("Hello2")

        channel.start_consuming()
    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
